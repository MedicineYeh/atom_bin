#!/bin/bash

sudo ./qemu/i386-softmmu/qemu -kernel bzImage -hda ubuntu.img -nographic -append "root=/dev/sda console=ttyS0 rw mem=1024M raid=noautodetect devtmpfs.mount=0 ip=dhcp" -smp 1 -m 1024m
