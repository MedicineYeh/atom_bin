#!/bin/bash

perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./BARNES.x86 < barnes_input
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./CHOLESKY.x86 input_cholesky/tk25.O -p1
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./FFT.x86 -m22 -n50 -l6 -p1
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./LU_con.x86 -n1024  -p1
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./LU_non.x86 -n1024 -p1
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./OCEAN_CON.x86 -p1 -n258
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./OCEAN_NON.x86 -p1 -n258
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./RADIX.x86 -p1 -n10000000
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./VOLREND.x86 1 input_volrend\/head
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./WATER-NSQUARED.x86 <water_input
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./WATER-SPATIAL.x86 < water_input
