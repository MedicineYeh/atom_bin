#!/bin/bash

perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./DCT.out 1.bmp
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./DCT_opt.out 1.bmp
