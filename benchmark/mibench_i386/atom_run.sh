#!/bin/bash

cd adpcm ;
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./rawcaudio.x86 < data/large.pcm > output_large.adpcm
cd .. ;
cd basicmath ;
rm -f tmp
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./basicmath_large.x86 > tmp
cd .. ;
cd bitcount ;
rm -f output_large.txt
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./bitcnts.x86 1125000 > output_large.txt
cd .. ;
cd blowfish ;
rm -f tmp1 tmp2
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./bf.x86 e input_large.asc output_large.enc 1234567890abcdeffedcba0987654321 > tmp1
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./bf.x86 d output_large.enc output_large.asc 1234567890abcdeffedcba0987654321 > tmp2
cd .. ;
cd CRC32 ;
rm -f output_large.txt
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./crc.x86 ../adpcm/data/large.pcm > output_large.txt
cd .. ;
cd dijkstra ;
rm -f output_large.dat
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./dijkstra_large.x86 input.dat > output_large.dat
cd .. ;
cd jpeg ;
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./cjpeg.x86 -dct int -progressive -opt -outfile output_large_encode.jpeg input_large.ppm
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./djpeg.x86 -dct int -ppm -outfile output_large_decode.ppm input_large.jpg
cd .. ;
cd patricia ;
rm -f output_large.txt
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./patricia.x86 large.udp > output_large.txt
cd .. ;
cd qsort ;
rm -f output_large.txt
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./qsort_large.x86 input_large.dat > output_large.txt
cd .. ;
cd sha ;
rm -f output_large.txt
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./sha.x86 input_large.asc > output_large.txt
cd .. ;
cd stringsearch ;
rm -f output_large.txt
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./search_large.x86 > output_large.txt
cd .. ;
cd susan ;
rm -f output_large.*
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./susan.x86 input_large.pgm output_large.smoothing.pgm -s
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./susan.x86 input_large.pgm output_large.edges.pgm -e
perf stat -e task-clock -e context-switches -e cpu-migrations -e page-faults -e cycles -e instructions -e cache-references -e cache-misses ./susan.x86 input_large.pgm output_large.corners.pgm -c
cd .. ;
