#!/bin/bash

sudo mount -o loop ubuntu.img tmp
sudo cp -r $1 tmp/root
sudo umount tmp
